#import "../templates/leçon.typ": *

#show: leçon.with()

= 01 --- Algorithmes d'ordonnancement et de gestion de ressources

== Motivation

- Problèmes du diner et du cocktail des philosophes
- Algorithme du banquier

== Gestion de ressources (space-multiplexed)

Ressources : Tout élément partagé entre plusieurs processus (mémoire, I/O, ...)

- Énoncé du problème
- Problématiques (vivacité, éviter la famine, fairness, ...)
- Quantité de ressources

- Implémentation de malloc (avec des free lists)
- Mutex, lock (accès concurents)
  + algorithme du boulanger de Peterson
  + interblocage

== Ordonnancement (time-multiplexed)

Le système a besoin d'ordre entre les processus pères et fils.

Critères : taux d'utilisation / progrès / fairness

=== Par lot

- First Come First Served
- SJF

=== Interactive systems

- Round Robin
- Priorité

=== Temps réel

- Statique
- Dynamique (EDF)

== Application aux OS

- Processus comme unité de gestion
- I/O (open, close, ...)
- Espace d'adressage (mémoire virtuelle)
