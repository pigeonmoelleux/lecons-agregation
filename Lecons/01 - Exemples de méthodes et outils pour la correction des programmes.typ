#import "../templates/leçon.typ": *

#show: leçon.with()

= 01 --- Exemples de méthodes et outils pour la correction des programmes
\
*Niveau* : L2/L3 #h(1fr) *Prérequis* : Bases de programmation

== Motivation

Les systèmes informatiques sont omniprésents dans la société : on veut pouvoir s'assurer que les programmes informatiques exécutés satisfont bien les spécifications attendues, que ce soit dans les interfaces utilisateurs, ou dans les systèmes critiques.

== Introduction

Attentes d'un programme :
- terminaison ;
- correction partielle et totale ;
- vérification de l'absence de bugs usuels ;
- documentation dans un projet réel avec prérequis et garanties.

== Terminaison

- Intuition
- Variants
- Pb de l'arrêt

== Correction partielle

=== Intuition

- Exemples triviaux pour montrer que les problèmes viennent des boucles et des appels récursifs
- Invariants de boucles et d'appels récursifs

=== Logique de Hoare

- Spécification (précondition/programme/postcondition)
- Comment prouver une spécification ? -> Système de preuve
- Distinguer le cas de la boucle en exposant le lien avec l'invariant de boucle
- Théorème de correction : *Développement*
- Théroème de complétude
- Théorème d'indécidabilité

== Outils de validation

=== Outils statiques

==== Typage

- Définitions des types et systèmes de typage
- "Well-typed programs cannot go wrong"
- Algorithme d'inférence de types : *Développement*

==== Propriété

- Présentation de la propriété en Rust et la comparaison avec C : *Développement*

=== Outils dynamiques

==== Tests

- TODO : trouver un développement en commun avec la leçon 3

==== Débuggers

- GDB
- valgrind