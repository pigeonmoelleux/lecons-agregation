#import "../templates/leçon.typ": *

#show: leçon.with()

= 17 --- Mémoire : du bit à l'abstraction vue par les processus.

== Introduction

- Définition de *mémoire vive* (RAM)

- Représentation binaire des données sur la machine

- Définition de bit/octet et préfixes associés

- Différences entre ROM/RAM/flash

== Architecture

- Stockage élémentaire d'un bit (registre 1 bit) : bascule D à front montant et commande d'écriture (DEV ?)

- Extension à des registres à n bits et à la RAM

- Communication RAM <-> CPU (bus)

- Latence de la RAM + présentation rapide des caches L1, L2, ...

- Transferts d'informations RAM <-> CPU + exemple d'écriture d'un mot

- Uniformité de la mémoire

- Registres CPU

== Systèmes

- Motivations de la structuration de la mémoire

- Protections nécessaires

- Processus : définition et présentation

- Stucturation de l'espace d'adressage
 + différences entre les modes U et S
 + Mode U : text / data / stack / env
 + Mode S : Mode U + kernel text / S stack / données systèmes propres au processus / données du kernel

- Partage de la mémoire entre différents processus -> Mémoire virtuelle : définition et présentation

- Correspondance mémoire physique/virtuelle
 + MMU
 + Tables d'indirection
 + Remplissage par des appels à malloc
 + Présentation des segfaults

- Gestion de la mémoire virtuelle -> Implémentation d'un allocateur mémoire : Free List / Buddy memory allocation (DEV)
