#let leçon(body) = {
  set page(
    paper: "a4",
    margin: (
      "left": 40pt,
      "right": 40pt,
      "top": 50pt,
      "bottom": 50pt,
    ),
    numbering: "1",
  )

  set heading(
    numbering: (..nums) => nums
    .pos()
    .slice(1)
    .map(str)
    .join(".") + " - ",
  )

  show heading: it => {
    if it.level == 1 {
      set align(center)
      set text(24pt)
      it.body
    } else if it.level == 2 {
      set text(22pt)
      counter(heading).display()
      it.body
    } else if it.level == 3 {
      set text(18pt)
      counter(heading).display()
      it.body
    } else if it.level == 4 {
      set text(16pt)
      counter(heading).display()
      it.body
    }
  }

  set par(justify: true)

  set text(lang: "fr", size: 16pt)

  body
}
