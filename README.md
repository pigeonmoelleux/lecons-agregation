# Leçons d'agrégation d'informatique

Ce dépôt contient ma préparation pour l'épreuve orale de leçon de l'agrégation d'informatique.

Il contient plus précisément [les plans de mes leçons](Lecons) ainsi que [mes développements](Developpements).

Tout est rédigé en [typst](https://typst.app), mais tous les fichiers compilés sont disponibles sur ma page perso Crans disponible ici : [https://perso.crans.org/pigeonmoelleux/Archives/Agreg/Leçons](https://perso.crans.org/pigeonmoelleux/Archives/Agreg/Leçons).