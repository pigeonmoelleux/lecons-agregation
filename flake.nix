{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    typst.url = "github:typst/typst/v0.8.0";
    typst-lsp.url = "github:nvarner/typst-lsp/v0.10.1";
    typstfmt.url = "github:jeffa5/typstfmt/b91d07033ef912451dde8e0cda4690316ff166ca";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, typst, typst-lsp, typstfmt, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      
      name = "Leçons";
      overlays = [ typst.overlays.default ];
      pkgs = import nixpkgs { inherit system; };

      in rec {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            typst.packages.${system}.default
            typstfmt.packages.${system}.default
            typst-lsp.packages.${system}.default
          ];

          shellHook = ''
            export PS1="\[\033[0;37m\][\[\033[0;31m\]\u\[\033[0;37m\]@\[\033[1;32m\]$(cat /etc/hostname)\[\033[0m\]:\[\033[0;34m\]\w\[\033[0;37m\]]\[\033[0;32m\] ${name} \[\[\033[0;37m\]$\[\033[0m\] "
          '';
        };

        packages.default =
          with pkgs;
          stdenv.mkDerivation rec {
            name = "lecons-agregation";
            src = self;
            buildPhase = ''
              for dir in {Developpements,Lecons}; do
                mkdir -p $out/$dir;
                  find "$src/$dir" -name "*.typ" | while read file; do ${pkgs.typst}/bin/typst --root . compile "$file" "$out/$dir/$(basename "$file" .typ).pdf"; done
              done
            '';
          };
      }
    );
}